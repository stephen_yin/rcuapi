<?php
/**
 * Demo 统一入口
 */
 //设置utf8
        header("Content-Type: text/html; charset=UTF-8");

// 指定允许其他域名访问
        header('Access-Control-Allow-Origin:*');
// 响应类型
        header('Access-Control-Allow-Methods:*');
// 响应头设置
        header('Access-Control-Allow-Headers:x-requested-with,content-type');
require_once dirname(__FILE__) . '/../init.php';

//装载你的接口
DI()->loader->addDirs('Butler');

/** ---------------- 响应接口请求 ---------------- **/

$api = new PhalApi();
$rs = $api->response();
$rs->output();

