<?php

class Model_Rc extends PhalApi_Model_NotORM {

    public function push($data) {
        $server = '192.168.1.2';
        $port = 3002;

        //获取pad端传过来的参数          
        $HEX = $data->hex;
           // $result = sendSocketMsg($server, $port, $HEX,1);
        //初次连接发送认证指令
       $rs = $s->sendmsg(hex2bin('FFFF00130000000000000000FFFFFFB300000000465553494f4e20504d5300'));
       if (!$rs) {
           throw new PhalApi_Exception_BadRequest('房控指令发送失败', 900);
       }
       $result = $s->sendmsg(hex2bin($HEX));     
        return $result;
    }



    /**
     *  邦奇房控
     * @param type $data
     * @throws PhalApi_Exception_BadRequest
     */
    public function push_bangqi($data) {
        $url = 'http://10.18.10.31:6000/third';
        $company = 'Dalitek';
        $seq = '1';
        $password = '123456';
        $datetime = date("Y-m-d H:i:s");
        $token = md5($company . $seq . $datetime . 'Dalitek&Third');
        $xml = '';

        if (!is_object($data)) {
            exit('Params data incorrect!');
        }

        switch ($data->type) {
            case 'tv':
            case 'tvonoff':
                if (!isset($data->hex) && $data->hex == '') {
                    exit('Can not find hex command!');
                }
                if (!isset($data->hex2) && $data->hex2 == '') {
                    exit('Can not find hex2 command!');
                }
                $hex = $data->hex;
                if ($data->state == 1) {
                    $hex = $data->hex;
                }
                $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <Request>
            <Auth company="' . $company . '" cseq="' . $seq . '" request_time="' . $datetime . '" token="' . $token . '"/>
            <Service business="ThirdControl" function="Control"/>
            <RequestData>
                <RoomName>' . $_GET['room'] . '</RoomName>
                <Password>' . $password . '</Password>
                <Command>' . $hex . '</Command>
            </RequestData>
        </Request>';
                break;

            case 'dnd':
            case 'light':
            case 'scene':
                if (!isset($data->hex) && $data->hex == '') {
                    exit('Can not find hex command!');
                }
                $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <Request>
            <Auth company="' . $company . '" cseq="' . $seq . '" request_time="' . $datetime . '" token="' . $token . '"/>
            <Service business="ThirdControl" function="Control"/>
            <RequestData>
                <RoomName>' . $_GET['room'] . '</RoomName>
                <Password>' . $password . '</Password>
                <Command>' . $data->hex . '</Command>
            </RequestData>
        </Request>';
                break;

            case 'curtain':

                $hex = $data->hex;
                if ($data->state == 1) {
                    $hex = $data->hex;
                }
                $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <Request>
            <Auth company="' . $company . '" cseq="' . $seq . '" request_time="' . $datetime . '" token="' . $token . '"/>
            <Service business="ThirdControl" function="Control"/>
            <RequestData>
                <RoomName>' . $_GET['room'] . '</RoomName>
                <Password>' . $password . '</Password>
                <Command>' . $hex . '</Command>
            </RequestData>
        </Request>';
                break;

            case 'curtainPause':
                if (!isset($data->open) && $data->open == '') {
                    exit('Can not find OPEN hex command!');
                }
                if (!isset($data->close) && $data->close == '') {
                    exit('Can not find CLOSE hex command!');
                }
                if (!isset($data->pause) && $data->pause == '') {
                    exit('Can not find PAUSE hex command!');
                }
                $hex = $data->hex;
                if ($data->state == 1) {
                    $hex = $data->hex;
                } elseif ($data->state > 1) {   // 暂停
                    $hex = $data->hex;
                }
                $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <Request>
            <Auth company="' . $company . '" cseq="' . $seq . '" request_time="' . $datetime . '" token="' . $token . '"/>
            <Service business="ThirdControl" function="Control"/>
            <RequestData>
                <RoomName>' . $_GET['room'] . '</RoomName>
                <Password>' . $password . '</Password>
                <Command>' . $hex . '</Command>
            </RequestData>
        </Request>';
                break;

            case 'fcu':
//                var_dump($data);
                if (!isset($data->box) && $data->box == '') {
                    exit('Can not find box param!');
                }
                if (!isset($data->airBox) && $data->airBox == '') {
                    exit('Can not find airBox param!');
                }
                if (!isset($data->fan) && $data->fan == '') {
                    exit('Can not find fan param!');
                }
                if (!isset($data->mode) && $data->mode == '') {
                    exit('Can not find mode param!');
                }
                if (!isset($data->temperature) && $data->temperature == '') {
                    exit('Can not find temperature param!');
                }

                $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <Request>
            <Auth company="' . $company . '" cseq="' . $seq . '" request_time="' . $datetime . '" token="' . $token . '"/>
            <Service business="ThirdControl" function="ControlAir"/>
            <RequestData>
                <RoomName>' . $_GET['room'] . '</RoomName>
                <Password>' . $password . '</Password>
                <Box>' . $data->box . '</Box>
                <AirBox>' . $data->airBox . '</AirBox>
                <Fan>' . $data->fan . '</Fan>
                <Mode>' . $data->mode . '</Mode>
                <SetT>' . $data->temperature . '</SetT>
            </RequestData>
        </Request>';
                break;
        }

        $result = '';
        if ($xml !== null) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            $result = curl_exec($ch);
            curl_close($ch);
            var_dump($result);
        } else {
            throw new PhalApi_Exception_BadRequest('XML Request is empty!',-40);
        }
        return $result;
    }
}
