<?php

/**
 * 房控接口
 */
class Api_Rc extends PhalApi_Api {

    public function getRules() {
        return array(
            'push' => array(
                'hex' => array('name' => 'hex', 'type' => 'string', 'require' => TRUE, 'desc' => '指令'),
            ),
            'push_bangqi' => array(
                'room' => array('name' => 'room', 'type' => 'string', 'require' => TRUE, 'desc' => '房间'),
                'type' => array('name' => 'type', 'type' => 'string', 'require' => TRUE, 'desc' => '类型'),
                'state' => array('name' => 'state', 'type' => 'string', 'require' => false, 'desc' => '状态'),
                'hex' => array('name' => 'hex', 'type' => 'string', 'require' => false, 'desc' => '指令'),
                 'hex2' => array('name' => 'hex2', 'type' => 'string', 'require' => false, 'desc' => '指令2'),     
                'open' => array('name' => 'open', 'type' => 'string', 'require' => false, 'desc' => '指令'),
                'box' => array('name' => 'box', 'type' => 'int', 'require' => false, 'desc' => '指令'),  
                'airBox' => array('name' => 'airBox', 'type' => 'int', 'require' => false, 'desc' => '指令'),  
                'fan' => array('name' => 'fan', 'type' => 'int', 'require' => false, 'desc' => '指令'),  
                'mode' => array('name' => 'mode', 'type' => 'int', 'require' => false, 'desc' => '指令'),  
                'temperature' => array('name' => 'temperature', 'int' => 'string', 'require' => false, 'desc' => '指令'),  
            ),
            'push_zb' => array(
                'hex' => array('name' => 'hex', 'type' => 'string', 'require' => TRUE, 'desc' => '指令'),
            ),
        );
    }

    /**
     * 房间控制系统INCOMM
     * @desc 用于获取修改过的列表
     * @return int ret 操作码，200表示成功
     * @return object data 用户列表
     * @return string msg 错误信息
     */
    public function push() {
        $domain = new Domain_Rc();
        $info = $domain->push($this);
        return $info;
    }
    
    
    /**
     * 房间控制系统(BANGQI)
     * @desc 用于获取修改过的列表
     * @return int ret 操作码，200表示成功
     * @return object data 用户列表
     * @return string msg 错误信息
     */
    public function push_bangqi() {
        $domain = new Domain_Rc();
        $info = $domain->push_bangqi($this);
        return $info;
    }

}
